import os
import sys
from pathlib import Path
import json
import requests
import sys


def get_tokens(cookies_path):
    if cookies_path.is_file():
        with open(cookies_path, "r", encoding="utf-8") as input_file:
            input_str = input_file.read().strip()
    else:
        raise FileNotFoundError("The file path does not exist:", file_path)
    return json.loads(input_str)


def write_data_file(day, year, name, cookie):
    # Create file directory
    local_dir =  f"data/{year}/{day:02}/"
    in_file_dir = Path.cwd() / f"{local_dir}"
    try: 
        os.makedirs(in_file_dir) 
    except OSError as error: 
        print(error) 
    
    # Check if the file already exists
    # AOC will rate limit you if you spam it, so best not to pull data we aready have
    file_name = f'{local_dir}day{day:02}_{name}.txt'
    file_path = Path.cwd() / f"./{file_name}"
    if file_path.is_file():
        print(f"WARNING: File already exists, skipping: {file_path}")
    else:
        # Get the data
        headers = {'session': cookie}
        url = f'https://adventofcode.com/2017/day/{day}/input'

        session = requests.Session()
        resp = session.get(url,cookies=headers)

        in_file = open(file_name,'w')
        in_file.write(resp.text)
        in_file.close() 


if __name__ == "__main__":
    current_path = os.path.dirname(os.path.abspath(__file__))
    sys.path.insert(0, current_path)

    INPUT_PATH_TOKENS = Path.cwd() / "./config/tokens.json"
    tokens = get_tokens(INPUT_PATH_TOKENS)

    for year in range(2015,2023):
        for day in range(1,25):
            for cookie_key, cookie_val in tokens.items():
                write_data_file(day=day, year=year, name=cookie_key, cookie=cookie_val)
